// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*";

function getLastCar(inventory) {
  if (Array.isArray(inventory)) {
    let lastCarInfo = inventory[inventory.length - 1];
    const lastCar = inventory.filter((car) => {
      if (car === lastCarInfo) {
        return car;
      }
    });
    return lastCar;
  } else {
    return [];
  }
}
module.exports = getLastCar;
