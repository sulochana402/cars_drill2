const inventory = require("../inventory");
const problem2 = require("../problem2");

const result = problem2(inventory);
console.log(`Last car is a ${result[0].car_make} ${result[0].car_model}`);
